<?php include('header.php'); ?>

    <main id="maincontent" class="padding">
        <div class="wrapper">
            <section id="introduction" class="section js-section" data-section-title="Introduction">
                <div class="">
                    <div class="header-top">
                        <h1>Hetain Patel <br> Trinity</h1><br/>
                        <h2>3 August to 30 October 2021</h2>
                    </div>
                    <p>John Hansard Gallery is proud to present <em>Trinity</em>, the largest solo exhibition to date by artist Hetain Patel. Developed over a number of years, this ambitious body of work brings together Patel&rsquo;s multiple interests and varied practice, often with collaboration at its core.</p>
                    <p><em>Trinity</em> focuses on the idea of connecting marginalised identities within the mainstream. Starting from an autobiographical stance, Patel uses humour and the languages of popular culture to highlight the familiar within the exotic, and recognition within the unknown. Patel has worked extensively with performers, creative partners, fabricators and specialists to develop the films and sculptural works within this exhibition.</p>
                    <p>At the heart of this exhibition is Patel&rsquo;s film trilogy, including the world premiere of the new and final part, <em>Trinity</em> (2021) that also gives the exhibition its title. Working with dance, martial arts and sign language collaborators, and with an especially composed score, <em>Trinity</em> represents Patel&rsquo;s most significant and developed film work so far.</p>
                    <p>Alongside the film <em>Trinity</em>, John Hansard Gallery will also present the first two highly acclaimed films from the trilogy: <em>Don&rsquo;t Look at the Finger</em> (2017) and <em>The Jump</em> (2015). The complete trilogy brings together the different facets of the rich filmic world the artist has been creating over the past six years. The exhibition will also feature a number of new sculptural works related to the trilogy, incorporating costumes and action figures from the films, as well as a film merchandise &lsquo;Gift Shop&rsquo;.</p>
                    <p>Taking inspiration from <em>Trinity</em>, John Hansard Gallery&rsquo;s Engagement and Learning team are organising a series of events and activities and will be sharing these over the coming weeks. Please see our website for the latest details.</p>
                    <p><em>Trinity</em> is presented in partnership with New Art Exchange, Nottingham, and will subsequently be shown there from 29 January to 24 April 2022.</p>
                    <ul class="logos">
                        <li class="thin"><a href="https://jhg.art/" target="_blank" rel="noopener"><img alt="John Hansard Gallery" src="assets/img/jhg-logo.svg" width="500" height="150" /> </a> </li>
                        <li class="wide"><a href="https://www.southampton.ac.uk/" target="_blank" rel="noopener"><img alt="University of Southampton" src="assets/img/UOS-logo.png" width="500" height="109" /> </a> </li>
                        <li class="wide"><a href="https://www.artscouncil.org.uk/" target="_blank" rel="noopener"><img alt="Arts Council England" src="assets/img/ACE-logo.png" width="500" height="118" /> </a> </li>
                        <li class="thin"><a href="http://www.nae.org.uk/" target="_blank" rel="noopener"><img alt="New Art Exchange" src="assets/img/nae_logo_black.png" width="500" height="292" /> </a> </li>
                    </ul>
                </div>
            </section>

            <section id="gallery1" class="section js-section" data-section-title="Gallery 1" data-section-subtitle="The Jump">
                <h2>Gallery 1<br>The Jump</h2>

                <div class="slider">
                    <!-- <figure class="slider__item">
                        <img src="assets/img/jump1.png" alt="" width="1000" height="527" />
                        <figcaption>Hetain Patel, Jump (2021)</figcaption>
                    </figure> -->
                    <figure class="slider__item">
                        <img src="assets/img/jump2.png" alt="" width="1000" height="527" />
                        <figcaption>Hetain Patel, <em>The Jump</em>, 2015</figcaption>
                    </figure>
                </div>

                <p><em>The Jump</em> (2015) brings together elements of widely recognised Hollywood action and superhero films within the domestic setting of Hetain Patel&rsquo;s grandmother&rsquo;s home, which has housed the artist and all of his relatives at various stages since 1967.</p>
                <p>Experienced as a 2-sided video installation, <em>The Jump</em> presents Patel leaping off the sofa in his grandmother&rsquo;s living room as his extended family looks on. His family are wearing formal Indian clothes, whilst he wears a homemade replica Spider-Man costume. According to Patel, this was the first sofa he had ever jumped off as a child whilst pretending to be the superhero. <em>The Jump</em> reflects on our collective desire for shared experiences and role models, but also his family&rsquo;s great leap, in reference to their migration to the UK.</p>
                <p>The semi-autobiographical film is shot in slow motion, which is at times so slow that <em>The Jump</em> could be considered as a moving photograph.</p>
                <p>The film was shot to imitate the production values of a big budget action movie and the original orchestral soundtrack by Patel&rsquo;s long term collaborator, composer and multi-instrumentalist, Amy May, takes the film further into the realm of epic cinema.</p>
                <p><em>The Jump</em> was commissioned by Wood Street Galleries, Pittsburgh, USA.</p>
                <div class="credits-table">
                    <p>A FILM BY HETAIN PATEL</p>
                    <div class="row">
                        <div class="column">
                            <p>Featuring: Pravin Patel, Leela Patel, Hetain Patel, Eva Martinez-Patel, Julie Perry, Oliver Perry, Lola Perry, Raphael Perry, Pritum Patel, Jayanti Patel, Ila Patel, Ila Patel, Dharmesh Patel, Kripa Patel, Ashok Patel, Beena Patel, Anish Patel, Leena Patel</p>
                        </div>
                        <div class="column">
                            <p><em>Creative Team</em><br />
                            Cinematographer: Sam St John<br />
                            Editor: Oliver Parker<br />
                            Colourist: Will Read Composer: Amy May<br />
                            Music Production: Lloyd Williams<br />
                            Camera Equipment: Quench Studios<br />
                            Gaffer & Lights: Drop City<br />
                            Electrician: Daniel Tunstall<br />
                            Stills photography: Pedro Labanca<br /><br />
                            Additional Thanks: Ian Duncan<br /><br />
                            Filmed on location: Patel Family Home, Bolton, UK</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="gallery2" class="section js-section" data-section-title="Gallery 2" data-section-subtitle="Don&rsquo;t Look at the Finger">
                <h2>Gallery 2<br>Don&rsquo;t Look at the Finger</h2>

                <div class="slider">
                    <figure class="slider__item">
                        <img src="assets/img/finger1.png" alt="" width="1000" height="563" />
                        <figcaption>Hetain Patel, <em>Don&rsquo;t Look at the Finger</em>, 2017</figcaption>
                    </figure>
                    <!-- <figure class="slider__item">
                        <img src="assets/img/finger2.png" alt="" width="1000" height="563" />
                        <figcaption>Hetain Patel, <em>Don&rsquo;t Look at the Finger</em> (2017)</figcaption>
                    </figure>
                    <figure class="slider__item">
                        <img src="assets/img/finger3.png" alt="" width="1000" height="563" />
                        <figcaption>Hetain Patel, <em>Don&rsquo;t Look at the Finger</em> (2017)</figcaption>
                    </figure> -->
                </div>

                <p><em>Don&rsquo;t Look at the Finger</em> (2017) features two main protagonists and a small group of companions, who have gathered in a church for a wedding ceremony. Through rituals, combat and signed languages, meaningful human communication is being sought, where each movement enhances the couples&rsquo; union.</p>
                <p>The film is a reflection on how symbols of historical cultural traditions and languages can become interspersed within today&rsquo;s diverse audio and visual landscape. <em>Don&rsquo;t Look at the Finger</em> also references the history and legacy of Hong Kong martial arts films, in particular Enter the Dragon (1973, dir. Robert Clouse). In a memorable scene, martial arts legend Bruce Lee warns us not to be distracted by a pointing finger, just in case we miss where we should actually be looking. Similarly, Patel is seemingly asking us to look in one direction, whilst the movements of the couple suggest that the real action is taking place elsewhere.</p>
                <p><em>Don&rsquo;t Look at the Finger</em> creates a fluid relationship between reality and fiction. The film embraces the UK, North America, South and East Asia, and West Africa as its culturally specific reference points, contrasted through the specially designed costumes, choreography, language and music that question their own origins.</p>
                <p><em>Don&rsquo;t Look at the Finger</em> was commissioned by Film and Video Umbrella, Manchester Art Gallery and QUAD. Supported by Arts Council England and Jerwood Choreographic Research Fund.</p>
                <div class="credits-table">
                    <p>A FILM BY HETAIN PATEL</p>
                    <div class="row">
                        <div class="column">
                            <p><em>Creative Team</em><br />
                            Executive Producer: Steven Bode, FVU<br />
                            Producer: Sophie Neave<br />
                            Production Supervisor: Susanna Chisholm, FVU<br />
                            Director of Photographer: Carlos Catalan<br />
                            Costume Designer: Holly Waddington<br /><br />

                            Fight Choreographer: Chirag Lukha<br />
                            Sign Language Consultant: Louise Stern<br />
                            Editor: Oliver Parker<br />
                            Music: Amy May<br /><br />

                            Featuring: Victoria Shulungu,<br />
                            Freddie Opoku-Addaie<br /><br />

                            Supporting Cast: Zoe Charlery, Vilma Jackson, Carol Lake, Claudette Little-Herron, Kadeem Oak, Thomas Owoo, Alicia Smikle-Little, Jessica Shulungu<br /><br />

                            Production Designer: Beck Rainford<br />
                            Hair & Make-up: Jessica Shulungu<br />
                            Costume Cutter & Maker: Kate Whitehead<br /><br />

                            1st Assistant Director: Chris Malin<br />
                            2nd Assistant Director: Sammy Wong<br />
                            3rd Assistant Director: Magda Kozak<br />
                            Production Assistant: Polly Wright, FVU<br /><br />

                            1st Assistant Camera: Sebastian Marczewki<br />
                            2nd Assistant Camera: Chai Rolfe<br />
                            Digital Image Technician: Jordan Wallace<br />
                            Steadicam Operator: Charlie Rizek<br />
                            Grips: Brett Lamerton, Myles Soldenhoff<br />
                            Gaffer: Antti Janhunen<br />
                            Spark: Jack Knot<br />
                            Additional Spark: Mike Johnson<br /><br />

                            Props Dressing: Tom Savery<br />
                            Standby: Aimee Alicia Meek<br /><br />

                            Costume Assistant: Rosie Stoward<br />
                            Additional Costume Makers: Elizabeth Brown, Florence Emiola<br /><br />

                            American Sign Language Interpreter: Helsa Borinstein<br />
                            Additional Sign Language Interpreters: Sophie Allen, Brett Best, Anna Kitson, Karen Newby, Daniel Roberts<br />
                            Stills Photographer: Nick Mathews</p>
                        </div>
                        <div class="column">
                            <p><em>Post Production</em><br />
                            Digital Intermediate: CHEAT<br />
                            Colourist: Joseph Bicknell<br />
                            Post Producer: Chloe Saunders<br />
                            Strings, Vocals & Composition: Amy May<br />
                            Percussion: Mathew Whittington, Real Percussion<br />
                            Cello & Mixing: Ben Trigg<br />
                            Flute: Hannah Lawrence<br />
                            Tuvan Singing: From live performance by Kaigal-Ool Kim-Oolovich Khovalyg<br /><br />

                            Sound by SoundNode<br />
                            Location Sound Recordist: David Crane<br />
                            Supervising Sound Editor: Martin Schulz<br />
                            Dubbing Mixers: Daniel Jaramillo, Martin Schulz<br /><br />

                            Foley by Clap Studios, Medellin<br />
                            Foley Artist: Daniel Giraldo<br />
                            Foley Recordist: Sebastian Alzate Lopez<br />
                            Foley Editor: Alexander Aguilar<br /><br />

                            Insurance supplied by Integro Entertainment<br />
                            Production Accountant: Sophie Luard, FVU<br /><br />

                            Camera Equipment supplied by ARRI Rental<br />
                            Lighting Equipment supplied by Panalux<br /><br />

                            Sound Equipment supplied by SoundNode<br />
                            Location Catering provided by CGI Catering<br />
                            Location provided by Stone Nest<br />
                            Rehearsal Studios provided by Sadler’s Well and ResCen<br /><br />

                            Special Thanks To: Bille Achilleos, Erik Eno Anderson, Chris Bannerman, Gema Beasley, Anna Billington, Sophie Bugeaud, Rob Garvie, Mike Jones, Pei Li Ng, Eva Martinez, Hannah Myers, Kate Pryor-Williams, Coraly Salamanca Stern, Inna Schorr, Richard Williamson, Students of Middlesex University<br /><br />
                            Filmed on location in London, United Kingdom</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="gallery3" class="section js-section" data-section-title="Gallery 3">
                <h2>Gallery 3</h2>

                <div class="slider">
                    <figure class="slider__item">
                        <img src="assets/img/pattern.png" alt="" width="1000" height="563" />
                        <figcaption>Hetain Patel, <em>Baa&rsquo;s House series</em>, 2021</figcaption>
                    </figure>
                </div>

                <p>&ldquo;I really want visitors to the exhibition to feel they are stepping into the kind of &lsquo;at the movies&rsquo; experience that I feel is still missing from mainstream society: the experience of being taken away by a trilogy of epic cinematic films, featuring complex culturally marginalised characters. Then I invite you to go behind the scenes, in a &lsquo;making of&rsquo; experience usually only afforded by big budgets films. Here you can get up close to the intricate costumes and hear from the collaborators, before finally &lsquo;exiting through the gift shop&rsquo;, where the marginalised worlds of the films unapologetically occupy the mainstream cultural space they are typically excluded from, intentionally taking physical, economic and political space via film posters, action figures and other merchandise. In a world where we as marginalised people, are typically given the soap drama, Eastenders treatment, rather than the big movie Batman treatment, we have to create our own.&rdquo;</p>
                <p>Hetain Patel<br /><br /></p>


                <p>This gallery features more of Hetain Patel&rsquo;s new figurative sculptural works and a mock &lsquo;Gift Shop&rsquo;. Some of the original costumes from the film trilogy are presented alongside action figure prototypes of the leading characters. Using the visual language of Hollywood merchandising, these artworks expand upon the ideas and visual motifs from the films. Through his works, Patel combines the personal and autobiographical with global issues of marginalisation and representation. The pattern from his grandmother&rsquo;s living room carpet that appears in the trilogy, has been incorporated onto the bespoke costumes and expanded to an epic scale on the gallery&rsquo;s exterior windows.</p>
                <p>Postcards, limited edition tote bags, and copies of the film posters are all available to buy from the JHG Shop downstairs. The Hetain Patel clothes designs shown here are also available to buy through the following website: <a target="_blank" href="https://www.contrado.co.uk/stores/baas-house-designs">www.contrado.co.uk/stores/baas-house-designs</a></p>
            </section>

            <section id="gallery4" class="section js-section" data-section-title="Barker-Mill Gallery" data-section-subtitle="Trinity">
                <h2>Barker-Mill Gallery<br>Trinity</h2>

                <div class="slider">
                    <!-- <figure class="slider__item">
                        <img src="assets/img/trinity1.png" alt="" width="1000" height="563" />
                        <figcaption>Hetain Patel, <em>Trinity</em> (2021)</figcaption>
                    </figure> -->
                    <figure class="slider__item">
                        <img src="assets/img/trinity2.png" alt="" width="1000" height="563" />
                        <figcaption>Hetain Patel, <em>Trinity</em>, 2021</figcaption>
                    </figure>
                </div>

                <p>Hetain Patel&rsquo;s new film <em>Trinity</em> (2021), centres on the discovery of an ancient fictional language that is a combination of signed languages and Kung-Fu. In this imaginary world, it is the intersecting lives of three women that lead to this discovery – Mina, a young British Indian woman, her mother, and Amy, a young Deaf garage worker.</p>
                <p>In Patel&rsquo;s fictional world, this corporal language that once united humanity is unintentionally rediscovered during the protagonists&rsquo; attempt to communicate with one another. As a consequence, their encounter sparks a &lsquo;body swap&rsquo;, enabling the pair to experience themselves in multiple avatars, as different genders, ethnicities, and body types. This body swapping is both a physical language of empathy, and a way to embody their ancestors – with the film proposing an idea of ancestry that expands beyond the confines of ethnicity.</p>
                <p><em>Trinity</em> is interspersed with visual references from both Patel&rsquo;s artistic practice and his Indian cultural heritage to explore the representation of the British Indian experience on screen. The women&rsquo;s interaction emphasises the female voice, intergenerational conflict and the notion that our bodies prevail beyond language, foregrounding a strong sense of hope. Intermingled with supernatural references, <em>Trinity</em> transforms traditional Indian practices through a recognisably Hollywood lens, employing a cinematic soundtrack and fight choreography.</p>
                <p><em>Trinity</em> creates space for cross-cultural costume design, created by Sarah Mercade, original music composed by Amy May, and dynamic choreography created with fight choreographer Chirag Lukha and Deaf artist and writer Louise Stern.</p>
                <p>The production of <em>Trinity</em> has been supported by Arts Council England, John Hansard Gallery, New Art Exchange, Sadler&rsquo;s Wells, Gulbenkian, Motwani Jadeja Family Foundation, British Art Show 9, and produced by Tilt Films.</p>
                <div class="credits-table">
                    <p>A FILM BY HETAIN PATEL</p>
                    <div class="row">
                        <div class="column">
                            <p>Written by Hetain Patel & In-Sook Chappell<br /><br />

                            <em>Creative Team</em><br />
                            Producer: Sophie Neave<br />
                            Director of Photography: Lorena Pagès<br />
                            Editor: Oliver Parker<br />
                            Music by: Amy May<br />
                            Production Designer: Bobbie Cousins<br />
                            Costume Designer: Sarah Mercade<br /><br />

                            Featuring: Vidya Patel – Mina,<br />
                            Sudha Bhuchar – Samanya (mum),<br />
                            Raffie Julien – Amy<br /><br />

                            The Ancestors: Saju Hari, Iris Chan, Melanie Ingram, Jaki Wilford<br />
                            Roshan: Nirmal Chohan<br /><br />

                            Hair & Make-up Designer: Cosima Crowley-Roth<br />
                            Sound Recordist: Jack Cook<br />
                            Choreographer: Chirag Lukha<br />
                            Sign Language & Story Consultant: Louise Stern<br />
                            Ancestors Casting: Eva Martinez<br /><br />

                            1st Assistant Director: Carlotta Beck Peccoz<br />
                            Production Manager: Carmen de Witt<br />
                            Production Assistant: Anna Argiros<br />
                            3rd Assistant Director: Sunjna Mullick Floor Runner: Zarife Sevin<br />
                            Additional Floor Runners: Gwennaëlle Counson, Amy Madden<br />
                            1st Assistant Camera: Riaz Ahmed, Rushil Choudhary, Chris Steel<br />
                            2nd Assistant Camera: Joe Salkey<br /><br />

                            Digital Image Technicians: Ellie C Bright, Gabriel Tineo<br />
                            Camera Trainees: Jake Phillips, Shayne Thomas-Gordon<br />
                            Steadicam Operator: James Thomas<br />
                            Gaffer: Krunal Saadrani<br />
                            Spark: Dimitrios Mavrogiannis<br />
                            Spark Dailies: Rob Gifford, Johnjoe Besagni<br />
                            Art Director: Sofia Sacomani<br />
                            Construction: Jasper Levine<br />
                            Graphics: Maria Sacomani<br />
                            Art Department Assistants: Hugo Harris, Phin Shaughnessy-Symons<br />
                            Costume Assistant: Anisha Fields<br />
                            Makeup & Hair Assistant: Amber Hoskins, Amy Luthwood-Graham<br />
                            Sign Language Interpreter: Jade Odle<br />
                            Covid-19 Supervisor: Wojciech Czarkowki<br />
                            Stills Photographer & BTS Videographer: Gabriel Tineo<br />
                            </p>
                        </div>
                        <div class="column">
                            <p><em>Post Production</em><br />
                            Colour and Finishing Services: CHEAT Colourist: Jack McGinty<br />
                            VFX Artist: Tim Mellem<br />
                            Violin: Charlie Brown<br />
                            Viola: Amy May<br />
                            Cello: Ben Trigg<br />
                            Vocals: Leela Patel, Verity Quade, Suzy Robinson, Amy May<br /><br />

                            Sound mixed at SoundNode<br />
                            Sound Designer: Martin Schulz<br />
                            Sound Editor: Daniel Jaramillo<br />
                            Re-Recording Mixer: David Crane<br /><br />

                            Foley by Clap Studios<br />
                            Foley Artist: Sebastián Vásquez Garzón<br />
                            Foley Recordist: Alexander Aguilar Castro<br />
                            Foley Editor: Juan Pablo Saldarriaga Alaguna<br />
                            Foley Supervisor: Daniel Jaramillo Gutierrez<br />
                            Studio Coordinator: Sebastián Alzate López<br />
                            Additional Music & Mixing: Matt Whittington<br /><br />

                            Camera Equipment supplied by FAVA Rental<br />
                            Lighting Equipment supplied by SHL Film & TV Lighting<br />
                            Location Catering provided by CGI Catering<br />
                            Rehearsal Studios provided by Sadler&rsquo;s Wells<br />
                            Insurance by Tysers<br /><br />

                            With Special Thanks to: Eva Martinez, Louise Tanoto, Laura Patay, Toke Broni Strandby, Julie Perry, Lolo Perry, Holly Waddington, Gwen Van Spijk, Cambridge Junction, Thomas Owoo, Callina Pearson, Queens Crescent Community Centre, Jessica Feely & Gustav<br /><br />

                            Filmed on location in Kentish Town and Hackney, London.<br /><br />
                            In memory of:<br />
                            Kunverben Patel (Mamai) 1918–2014<br />
                            Lakshmiben Patel (Baa) 1928–2017<br /><br />

                            A TILT FILMS PRODUCTION
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="abouttheartist" class="section js-section" data-section-title="About the Artist">
                <h2>About the Artist</h2>
                <p>Hetain Patel (b. 1980, Bolton) is an artist whose practice explores the subtle and often humorous complexities of identity formation. Since 2004, Patel&rsquo;s works have been shown nationally and internationally within institutions such as Tate Britain, London, and the Ullens Centre for Contemporary Art, Beijing. John Hansard Gallery has a long history of working with Patel, having first worked with him in partnership with Art Asia in 2012, and again as part of <em>JHG Sampler</em> in 2018.</p>
                <p>Patel&rsquo;s practice is varied and wide-reaching, ranging from performing Bruce Lee impersonations at the Royal Opera House, completing performance commissions for Tate Modern and Southbank Centre, and making a working class Transformer robot from an old Ford Fiesta car with his dad (shown at <em>JHG Sampler</em> in 2018). Patel toured his live performance, TEN, internationally and was invited to do a TED Talk which has since been viewed globally over two million times.</p>
                <p>Patel&rsquo;s film <em>Don&rsquo;t Look at the Finger</em> has recently been purchased by TATE and the British Council. He is a Sadler&rsquo;s Wells New Wave Associate Artist, and winner of the 2019 Film London Jarman Award. Patel is also part of British Art Show 9 which is touring the UK throughout 2021–22.</p>
                <p><a target="_blank" href="http://hetainpatel.com">hetainpatel.com</a></p>
            </section>

            <section id="behindthescenes" class="section js-section" data-section-title="Behind the Scenes">
                <h2>Behind the Scenes</h2>
                <figure class="slider">
                    <div class="plyr__video-embed player" id="player">
                        <iframe
                            src="https://player.vimeo.com/video/579345041?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media"
                            allowfullscreen
                            allowtransparency
                            allow="autoplay"
                        ></iframe>
                    </div>
                    <figcaption>Hetain Patel, <em>Don&rsquo;t Look at the Finger</em>, 2017</figcaption>
                </figure>
                <figure class="slider">
                    <div class="plyr__video-embed player" id="player">
                        <iframe
                            src="https://player.vimeo.com/video/579344680?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media"
                            allowfullscreen
                            allowtransparency
                            allow="autoplay"
                        ></iframe>
                    </div>
                    <figcaption>Hetain Patel, <em>The Jump</em>, 2015</figcaption>
                </figure>

                <div class="slider">
                    <div class="slider__item" >
                        <img src="assets/img/JHG_Placeholder.png" alt="" width="1000" height="527" />
                        <figcaption>Hetain Patel, <em>Trinity</em>, 2021</figcaption>
                    </div>
                </div>
            </section>

            <!-- <section id="credits" class="section js-section" data-section-title="Credits">
                <h2>Credits</h2>
                <p><em>Trinity</em> is presented in partnership with New Art Exchange, Nottingham, and will be shown there from 29 January – 24 April 2022. <em>Trinity</em> has been financially supported by Arts Council England, John Hansard Gallery, New Art Exchange, Sadler&rsquo;s Wells, Gulbenkian, Motwani Jadeja Family Foundation, British Art Show 9, and produced by Tilt Films.</p>
                <p><em>Don&rsquo;t Look at the Finger</em> was originally commissioned by Film and Video Umbrella with Manchester Art Gallery, QUAD and supported by the Jerwood Choreographic Research Fund. <em>The Jump</em> was originally commissioned by Wood Street Galleries, Pittsburgh, USA.</p>
                <div class="credit"><a href="https://www.jhg.art" target="_blank" rel="noopener">www.jhg.art</a></div>
                <div class="credit"><a href="https://www.nae.org.uk" target="_blank" rel="noopener">www.nae.org.uk</a></div>
                
            </section>

            <section id="engagement" class="section js-section" data-section-title="Engagement and Events Programme">
                <h2>Engagement and Events Programme</h2>
                <p><b>Wednesday lunchtime talks</b><br>
                    <b>Throughout the exhibition</b><br>
                    <b>Drop-in, meet in foyer at 1pm</b><br>
                    A series of ten-minute talks by John Hansard Gallery&rsquo;s Gallery Assistants and other members of staff on an aspect of their choice from the exhibition. Gallery Assistant&rsquo;s will discuss anything from their most or least favourite artwork in the exhibition, their interpretation of the exhibition, or interesting exchanges they have had with the public during the show.</p>
            </section> -->
        </div>
    </main>

<?php include('footer.php'); ?>