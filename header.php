<!doctype html>
<html lang="en">

<head>
    <title>Hetain Patel - Trinity</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Meta content here">
    <link rel="stylesheet" href="assets/css/main.css">
    <script src="https://cdn.plyr.io/3.6.8/plyr.polyfilled.js"></script>
    <link rel="stylesheet" href="https://cdn.plyr.io/3.6.8/plyr.css" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png"/>
</head>

<body>

<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- <a href="#maincontent" class="skip-link">Skip to main content</a> -->

<header>

    <nav class="menu js-section-menu is-collapsed">
        <ul>
<!--            <li><a href="#link1">Extra Link #1</a></li>-->
<!--            <li><a href="#link2">Extra Link #2</a></li>-->
        </ul>
        <span class="active-section">
            <ul>

            </ul>
        </span>
        
    </nav>

</header>